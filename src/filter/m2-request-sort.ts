export class M2RequestSort {

    sortingField: string = null;
    sortingCondition: string = null;

    constructor(field: string, condition: string){
        this.sortingField = field;
        this.sortingCondition = condition;
    }

    clear(){
        this.sortingField = null;
        this.sortingCondition = null;
    }
}
