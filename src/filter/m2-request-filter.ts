export class M2RequestFilter {

    public static EQ      = 'eq';        //Equals.
    public static FINSET  = 'finset';	 //A value within a set of values
    public static FROM	  = 'from';      //The beginning of a range. Must be used with to
    public static GT	  = 'gt';        //Greater than
    public static GTEQ	  = 'gteq';      //Greater than or equal
    public static IN	  = 'in';        //In. The value can contain a comma-separated list of values.
    public static LIKE	  = 'like';      //Like. The value can contain the SQL wildcard characters when like is specified.
    public static LT	  = 'lt';        //Less than
    public static LTEQ	  = 'lteq';      //Less than or equal
    public static MOREQ	  = 'moreq';     //More or equal
    public static NEQ	  = 'neq';       //Not equal
    public static NIN	  = 'nin';       //Not in. The value can contain a comma-separated list of values.
    public static NOTNULL = 'notnull';	 //Not null
    public static NULL	  = 'null';      //Null
    public static TO	  = 'to';        //The end of a range. Must be used with from

    field: string;
    value: string;
    condition_type: string;

    constructor(field: string, value: string, condition_type: string){
        this.field = field;
        this.value = value;
        this.condition_type = condition_type;
    }

    clear(){
        this.value = null;
        this.condition_type = null;
    }
}
