import { M2RequestSort } from './m2-request-sort';
import { sprintf } from 'sprintf';
import { HttpParams } from '@angular/common/http';
import { M2RequestFilterGroup } from './m2-request-filter-group';
import { isNullOrUndefined } from 'util';
import { M2RequestFilter } from './m2-request-filter';

export class M2RequestGenerator {
    filter_groups: M2RequestFilterGroup[];
    sorting: M2RequestSort[];
    pageCurrent: number = null;
    pageSize: number = null;

    constructor(){
        this.filter_groups = [];
        this.clear();
    }

    addFilterGroup(group: M2RequestFilterGroup){
        this.filter_groups.push(group);
    }

    addSorting(sort: M2RequestSort){
        this.sorting.push(sort);
    }

    deleteSorting(field: string){
        const index = this.sorting.findIndex(el => el.sortingField === field);
        if (index > -1) {
            this.sorting.splice(index, 1);
        }
    }

    clear(){
        this.filter_groups = [];
        this.sorting = [];

        this.pageCurrent = 1;
        this.pageSize = 10;
    }

    getParams(): HttpParams {
        let reqParams = new HttpParams();

        if (!isNullOrUndefined(this.pageSize)){
            reqParams = reqParams.set('searchCriteria[pageSize]', this.pageSize.toString());
        }
        if (!isNullOrUndefined(this.pageCurrent)){
            reqParams = reqParams.set('searchCriteria[currentPage]', this.pageCurrent.toString());
        }

        if (this.sorting.length > 0){
            this.sorting.forEach((item, index) => {
                reqParams = reqParams.set(sprintf('searchCriteria[sortOrders][%d][field]', index), item.sortingField);
                reqParams = reqParams.set(sprintf('searchCriteria[sortOrders][%d][direction]', index), item.sortingCondition);
            });
        }

        if (this.filter_groups.length > 0){
            this.filter_groups.forEach((group, group_index) => {
                group.filters.forEach((filter, filter_index) => {
                    reqParams = reqParams.set(sprintf('searchCriteria[filter_groups][%d][filters][%d][field]', group_index, filter_index), filter.field);
                    reqParams = reqParams.set(sprintf('searchCriteria[filter_groups][%d][filters][%d][condition_type]', group_index, filter_index), filter.condition_type);
                    switch (filter.condition_type){
                        case M2RequestFilter.LIKE:
                            reqParams = reqParams.set(sprintf('searchCriteria[filter_groups][%d][filters][%d][value]', group_index, filter_index), '%' + filter.value + '%');
                            break;
                        default:
                            reqParams = reqParams.set(sprintf('searchCriteria[filter_groups][%d][filters][%d][value]', group_index, filter_index), filter.value);
                            break;
                    }

                });
            });
        }

        return reqParams;
    }

}