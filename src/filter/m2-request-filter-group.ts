import { M2RequestFilter } from './m2-request-filter';

export class M2RequestFilterGroup {

    filters: M2RequestFilter[];

    constructor(){
        this.filters = [];
    }

    addFilter(filter: M2RequestFilter){
        this.filters.push(filter);
    }

    rmFilter(field:string){
        const index = this.filters.findIndex(el => el.field === field);
        if (index > -1) {
            this.filters.splice(index, 1);
        }
    }

    clear(){
        this.filters = [];
    }
}
